const express = require('express');
const axios = require('axios');
const moment = require('moment');
const _ = require('lodash');
const nodemailer = require("nodemailer");
const getcallapi = require('../utils/externalapi');
const appDatas = require('../data/lineapprove');
const router = express.Router();

router.get('/', (req, res) => {
    res.json({ data : 'Hello'});
});

router.get('/page', async (req,res) => {
    const config = {
        method: 'get',
        url: 'http://3.1.189.234:8090/data/ttntest',
        headers: {}
    };
    const page = 1;
    const limit = 200;
    var itemArr = [];
    axios(config)
        .then((r) => { 
            var tgArray = [];
            tgArray.push(r.data);
            const result = _(tgArray[0])
                    .orderBy(['data:'], ['asc']) 
                    .drop((page - 1) * limit)   
                    .take(limit)              
                    .value();
            const min = Math.min.apply(null, tgArray[0].map(function(item) {
                return item.id;
            }))
            const max = Math.max.apply(null, tgArray[0].map(function(item) {
                return item.id;
            }));
    
            const getdata = tgArray[0].map(x => x.id);
    
            let indx = Object.keys(tgArray[0]).length;
            console.log('idx : ', indx);

            let avgs = (max + min) / indx;
           
         
            const filtered = result.map(d => d.timestamp);
            filtered.forEach(d => {
                let times = moment(d).unix(); //.format('h:mm:ss')
                console.log('time : ', times);
            });
            const total = tgArray[0].length;
            const resuleData = { 
                max: max,
                min: min,
                avg: avgs,
                total: total,
                data: result
            }
    
            console.log(resuleData);
            res.json(resuleData);
        })
        .catch((error) => {
            console.log(error);
        });
});

router.get('/getApprove', (req, res) => {
    res.json(appDatas);  
});

router.post('/Approve', (req, res) => {
    let myData = [];
    // id: req.body.id,
    // name: req.body.name,
    // position: req.body.position,
    // isApprove: req.body.isApprove,
    let items = {
        ...req.body
    }
    //console.log(items);
    appDatas.push(items);
    appDatas.forEach(e => {
        let userOneApprove = false;
        let userTwoApprove = false;
        let userThreeApprove = false;
        if(e){
            if(e.name = 'user1'){
                if(e.isApprove = 1){
                    userOneApprove = true;
                }else{
                    userOneApprove = false;
                }
            }
            if(e.name = 'user2'){
                if(e.isApprove = 1){
                    userTwoApprove = true;
                }else{
                    userTwoApprove = false;
                }
            }
            if(e.name = 'user3'){
                if(e.isApprove = 1){
                    userThreeApprove = true;
                }else{
                    userThreeApprove = false;
                }
            }

            //check is approve
            if(userOneApprove === true && userTwoApprove === true && userThreeApprove === true){
                let testAccount = nodemailer.createTestAccount();
                let transporter = nodemailer.createTransport({
                    host: "smtp.mailtrap.io",
                    port: 2525,
                    secure: false,
                    auth: {
                        user: "59bcd31228c4d3",
                        pass: "8238663819b10f"
                    },
                  });

                let info = transporter.sendMail({
                    from: 'noreply@example.com',
                    to: e.email,
                    subject: "Approve job ✔", 
                    text: "Please confirm", 
                    html: "<b>confirm your app</b>", 
                });
                console.log("Message sent: %s", info.messageId);
                console.log('complete approve');
            }else{
                console.log('Not complete approve');
            }

      
         
        }
    });
    res.json(appDatas); 
});

module.exports = router;