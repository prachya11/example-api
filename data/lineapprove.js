const aproveData = [
    {
        id: 1,
        name: "user1",
        email: "test1@gmail.com",
        position: "president",
        isApprove: 1
    },
    {
        id: 2,
        name: "user2",
        email: "test2@gmail.com",
        position: "manager",
        isApprove: 1
    },
    {
        id: 3,
        name: "user3",
        email: "test3@gmail.com",
        position: "acquisition specialist",
        isApprove: 1
    }
];

module.exports = aproveData;
