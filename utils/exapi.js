const axios = require('axios');

const config = {
  method: 'get',
  url: 'http://3.1.189.234:8090/data/ttntest',
  headers: {}
};

const getTargetData = () => {
  const targetArr = [];
  return new Promise(async (resolve, reject) => {
    try {
      const result = axios(config);
      if (result.data.errors.length > 0) {
        return reject(result.data.errors);
      }
      return resolve(result.data);
    } catch (error) {
      console.log(error);
      return reject(error);
    }
  });
}



module.exports = { getTargetData }
