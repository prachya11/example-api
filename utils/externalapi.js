const axios = require('axios');
const moment = require('moment');
const _ = require('lodash');

const config = {
  method: 'get',
  url: 'http://3.1.189.234:8090/data/ttntest',
  headers: {}
};

const getDataApi = (pages,limits) => {

    var itemArr = [];
    const page = pages;
    const limit = limits;
    axios(config)
        .then((r) => { 
            var tgArray = [];
            tgArray.push(r.data);
            const result = _(tgArray[0])
                    .orderBy(['data:'], ['asc']) 
                    .drop((page - 1) * limit)   
                    .take(limit)              
                    .value();
            const min = Math.min.apply(null, tgArray[0].map(function(item) {
                return item.id;
            }))
            const max = Math.max.apply(null, tgArray[0].map(function(item) {
                return item.id;
            }));
    
            const getdata = tgArray[0].map(x => x.id);
    
            let indx = Object.keys(tgArray[0]).length;

            let avg = (max + min) / indx;
            console.log('idx : ', indx);
         
            const filtered = result.map(d => d.timestamp);
            filtered.forEach(d => {
                let times = moment(d).unix(); //.format('h:mm:ss')
                console.log('time : ', times);
            });
            const total = tgArray[0].length;
            const resuleData = { 
                max: max,
                min: min,
                avg: avg,
                total: total,
                data: result
            }
    
            console.log(resuleData);
            return resuleData;
        })
        .catch((error) => {
            console.log(error);
        });
}
module.exports = {getDataApi}
