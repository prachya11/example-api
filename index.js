const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const index = require('./routes/index');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({    
    extended: true 
}));

app.use('/', index);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});